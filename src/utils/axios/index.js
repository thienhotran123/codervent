export const getResponseError = (err) => {
  let result = {
    errorCode: 'UNKNOWN CODE',
    errorMessage: 'UNKNOWN MESSAGE',
  };
  const {response} = err;

  if (response?.data) {
    const errorMes = JSON.parse(response.data);
    result = {
      errorCode: err.code,
      errorMessage: errorMes.errorMessage,
    };

    if (!errorMes.errorMessage) {
      result = {
        ...result,
        errorMessage: 'Incorrect Login',
      };
    }
  }
  return result;
};
