import { LOCAL_KEY } from "../../constants";
import axiosInstance from "../../helpers/apiClient";
import { getResponseError } from "../../utils/axios";

export const loginAPI = async (data) => {
    let result = {
      errorCode: 'UNKNOWN CODE',
      errorMessage: 'UNKNOWN MESSAGE',
    };
  
    try {
      const res = await axiosInstance.post('/user',
        data,
      );
      const dataUser = JSON.parse(res.data);
  
      const dataSaveToLocal = {
        ...dataUser,
        ...data,
      };

      localStorage.setItem(LOCAL_KEY, JSON.stringify(dataSaveToLocal));
      result = {};
    } catch (error) {
      result = getResponseError(error);
    }
    return result;
  };
  