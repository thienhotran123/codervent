import React, { useRef } from 'react';
import { useFormik } from "formik";
import * as Yup from "yup";
import { validateCaptcha } from "react-simple-captcha";
import { useHistory } from 'react-router-dom';

import './signIn.css'
import InputField from './components/InputField';
import { loginAPI } from '../../../services/authorization';
import Captcha from './components/Captcha';
import { useEffect } from 'react';
import { LOCAL_KEY } from '../../../constants';

const SignIn = () => {
  const captchaRef = useRef(null);
  const navigate = useHistory();

  useEffect(() => {
    const isLogin = localStorage.getItem(LOCAL_KEY);
    isLogin && navigate.push('/Home');
  }, [navigate]);

  const initialValues = {
    Username: "",
    Password: "",
    Captcha: "",
  };

  const signUpSchema = Yup.object().shape({
    Username: Yup.string().min(3).max(20).required("Please Enter your name"),
    Password: Yup.string().min(3).required("Please Enter your Password"),
    Captcha: Yup.string().min(4).required("Please Enter the Captcha"),
  });

  const { values, errors, touched, handleBlur, handleChange, handleSubmit } = useFormik({
    initialValues: initialValues,
    validationSchema: signUpSchema,
    onSubmit: (values) => {
      handleLogin(values);
    },
  });

  const handleLogin = async () => {
    if (!values.Username && !values.Password && !captchaRef.current.value) { return }

    const captcha = captchaRef.current.value;
    if (validateCaptcha(captcha)) {
      const dataToLogin = {
        username: values.Username,
        password: values.Password,
        captcha: values.Captcha
      }
      const dataLogin = await loginAPI(dataToLogin)

      if (!dataLogin.errorCode) {
        navigate.push('/Home');
      } else {
        alert(dataLogin.errorMessage)
      }
    }
    else {
      captchaRef.current.value = ''
    }
  }

  return (
    <div>
      <main class="authentication-content">
        <div class="container-fluid">
          <div class="authentication-card">
            <div class="card shadow rounded-0 overflow-hidden">
              <div class="row g-0">
                <div class="col-lg-6 bg-login d-flex align-items-center justify-content-center">
                  <img src="assets/images/error/login-img.jpg" class="img-fluid" alt="" />
                </div>
                <div class="col-lg-6">
                  <div class="card-body p-4 p-sm-5">
                    <h5 class="card-title">Sign In</h5>
                    <p class="card-text mb-5">See your growth and get consulting support!</p>
                    <div class="form-body" action="">
                      <div class="row g-3">
                        <InputField
                          error={errors.Username}
                          handleBlur={handleBlur}
                          id='inputUsername'
                          name="Username"
                          handleChange={handleChange}
                          touched={touched.Username}
                          value={values.Username}
                          icon={<i class="bi bi-person-fill"></i>}
                          label='Username'
                          type='text'
                        />
                        <InputField
                          error={errors.Password}
                          handleBlur={handleBlur}
                          id='inputPassword'
                          name="Password"
                          handleChange={handleChange}
                          touched={touched.Password}
                          value={values.Password}
                          icon={<i class="bi bi-lock-fill"></i>}
                          label='Password'
                          type='password'
                        />
                        {touched.Password && touched.Username &&
                          <Captcha
                            error={errors.Captcha}
                            handleBlur={handleBlur}
                            handleChange={handleChange}
                            captchaRef={captchaRef}
                            touched={touched.Captcha}
                          />}
                        <div class="col-6">
                          <div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" checked="" />
                            <label class="form-check-label" for="flexSwitchCheckChecked">Remember Me</label>
                          </div>
                        </div>
                        <div class="col-6 text-end">	<a href="authentication-forgot-password.html">Forgot Password ?</a>
                        </div>
                        <div class="col-12">
                          <div class="d-grid">
                            <button type="submit" class="btn btn-primary radius-30" onClick={handleSubmit} enable="true">Sign In</button>
                          </div>
                        </div>
                        <div class="col-12">
                          <p class="mb-0">Don't have an account yet? <a href="authentication-signup.html">Sign up here</a></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default SignIn;