const InputField = ({ name, id, value, handleChange, handleBlur, error, touched, icon, label, type }) => {

    return (
        <div class="col-12">
            <label for={id} class="form-label">{label}</label>
            <div class="ms-auto position-relative">
                <div class="position-absolute top-50 translate-middle-y search-icon px-3">{icon}</div>
                <input name={name} type={type} class="form-control radius-30 ps-5" id={id} placeholder={name} value={value} onChange={handleChange} onBlur={handleBlur} />
            </div>
            <p className="form-labe form-error">{error && touched ? error  : ' '}</p>
        </div>
    )
}

export default InputField;