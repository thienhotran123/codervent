import { useEffect } from "react";
import { loadCaptchaEnginge, LoadCanvasTemplate } from "react-simple-captcha";

const Captcha = ({ handleChange, handleBlur, error, touched, captchaRef }) => {

    useEffect(() => {
        loadCaptchaEnginge(4);
    }, []);

    return (
        <div>
            < LoadCanvasTemplate reloadText="Reload My Captcha" reloadColor="blue" />
            <div className="input-block">
                <input type="text" ref={captchaRef} id="captcha" name="Captcha" placeholder="Enter the Captcha" onChange={handleChange} onBlur={handleBlur} className='btn btn-primar btn-captcha' />
                {error && touched ? <p className="form-labe form-error">{error}</p> : null}
            </div>
        </div>
    )

}

export default Captcha;
