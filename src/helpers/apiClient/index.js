import axios from 'axios';
import interceptorConfig from './apiConfigs/index';
import {parse, stringify } from 'qs';

const instance = axios.create({
  baseURL: 'https://63fc2ebb677c41587307350c.mockapi.io',
  responseType: 'json',
  headers: {
    'Content-Type': 'application/json',
  },
  transformResponse: (r) => r,
  paramsSerializer: {
    encode: parse,
    serialize: stringify,
  },
});

instance.interceptors.request.use(
  interceptorConfig.requestInterceptor,
  interceptorConfig.requestError,
);

instance.interceptors.response.use(
  interceptorConfig.responseInterceptor,
  interceptorConfig.responseError,
);

const axiosInstance = instance;

export default axiosInstance;
