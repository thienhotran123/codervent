export const requestError = (error) => {
  if (error.code === 'ECONNABORTED') {
    const errorRes = {
      type: 'REQUEST TIMEOUT',
      errorMessage: 'REQUEST TIMEOUT',
    };

    throw errorRes;
  }
};
