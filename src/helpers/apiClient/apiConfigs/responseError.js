export const responseError = async (error) => {
    if (error.code === 'ERR_CANCELED') {
        //Error handling when unable to connect to the Internet
        throw error;
    } else if (error.response) {
        const { status, config } = error.response;

        switch (status) {
            case 400:
                if (!config.url) {
                    alert(error.message);
                }
                break;

            case 401:
                //Handle when not have permission
                if (!config.url) {
                    alert('Handle when not have permission');
                }
                break;

            default:
                break;
        }

        return Promise.reject(error);
    } else if (error.request) {
        if (error.code === 'ECONNABORTED') {
            alert('Please review the connection settings and try again.');
        } else if (error.code === 'ERR_BAD_RESPONSE') {
            throw error;
        }
    }
};
