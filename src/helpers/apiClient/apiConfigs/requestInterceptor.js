import { LOCAL_KEY } from "../../../constants";

export const requestInterceptor = async (config) => {
    config.timeout = 30000;

    const dataLocal = localStorage.getItem(LOCAL_KEY);

    if (dataLocal) {
      const access_token = JSON.parse(dataLocal)

      //config headers when call API to server
      
      // config.headers = {
      // ...config.headers,
      // Authorization: `Bearer ${dataLocal.access_token}`,
      // TimeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
      // };
    }

  return config;
};
